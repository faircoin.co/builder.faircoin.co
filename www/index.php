<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>FairCoin.Co - App Builder!</title>
    <link href="/assets/css/Ubuntu/stylesheet.css" rel="stylesheet">
  </head>
<body>
<style>
  body {
    font-family: 'Ubuntu Mono';
  }
  h1 {
    text-align: center;
  }
  table {
    padding: 50px;
    border-
  }
  th, td {
    padding-left: 10px;
    padding-right: 10px;
    border-bottom: 1px solid gainsboro;
  }

  table tr td:nth-of-type(1) {
    text-align: right;
  }

  p {
    text-align: center;
  }
</style>
<h1>FairCoin.Co - App Builder</h1>
<p>( Please use the official releases for <a href="https://faircoin.co/download">download</a> ! )</p>
<table>
  <tr><th>creation time<br>filesize</th><th>binary</th><th>sha256sum</th></tr>
<?php

$F=glob('./bin/ElectrumFair*');
foreach($F as $f){
  echo '<tr><td>'.date("Y-m-d H:i", filemtime($f) ).'<br>'.number_format( filesize($f),0,',','.').' B<br></td><td><a href="'.$f.'">'.$f.'</a></td><td>'.hash_file('sha256', $f).'</td></tr>';
}

?>
</table>
</body>
