#!/bin/bash
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`

case $1 in
  "build-electrumfair-tgz")
      docker-compose stop electrumfair-tgz
      mkdir -p ~/bin
      git clone https://github.com/faircoin/electrumfair.git
      docker-compose build electrumfair-tgz
      docker-compose up -d --remove-orphans electrumfair-tgz
      docker-compose exec -T electrumfair-tgz python3 -m pip install --system -r contrib/deterministic-build/requirements.txt -t contrib/../packages
      docker-compose exec -T electrumfair-tgz contrib/make_tgz
      docker-compose exec -T electrumfair-tgz rsync -arv --include=ElectrumFair*.zip --include=ElectrumFair*.tar.gz --exclude=* /home/faircoin/electrumfair/dist/ /home/faircoin/bin
    ;;
  "build-electrumfair-appimage")
      mkdir -p ~/bin
      git clone https://github.com/faircoin/electrumfair.git
      docker-compose stop electrumfair-appimage
      docker-compose build electrumfair-appimage
      docker-compose up -d --remove-orphans electrumfair-appimage
      docker-compose exec -T electrumfair-appimage contrib/build-linux/appimage/build.sh
      docker-compose exec -T electrumfair-appimage rsync -arv --include=ElectrumFair*.AppImage --exclude=* /home/faircoin/electrumfair/dist/ /home/faircoin/bin
    ;;
  "build-electrumfair-apk")
      git clone https://github.com/faircoin/electrumfair.git
      cd electrumfair && contrib/make_packages
      mkdir --parents ./.buildozer/.gradle
      docker-compose stop electrumfair-apk
      docker-compose build electrumfair-apk
      docker-compose up -d --remove-orphans electrumfair-apk
      docker-compose exec -T electrumfair-apk contrib/android/make_apk
      docker-compose exec -T electrumfair-apk rsync -arv --include=ElectrumFair*.apk --exclude=* /home/user/wspace/electrumfair/bin/ /home/user/bin
    ;;
  "build-electrumfair-apk-release")
      git clone https://github.com/faircoin/electrumfair.git
      cd electrumfair && contrib/make_packages
      mkdir --parents ./.buildozer/.gradle
      docker-compose stop electrumfair-apk
      docker-compose build electrumfair-apk
      docker-compose up -d --remove-orphans electrumfair-apk
      docker-compose exec -T electrumfair-apk contrib/android/make_apk release
      docker-compose exec -T electrumfair-apk rsync -arv --include=ElectrumFair*.apk --exclude=* /home/user/wspace/electrumfair/bin/ /home/user/bin
    ;;
  "build-electrumfair-win")
      mkdir -p ~/bin
      git clone https://github.com/faircoin/electrumfair.git
      docker-compose stop electrumfair-win
      docker-compose build electrumfair-win
      docker-compose up -d --remove-orphans electrumfair-win
      docker-compose exec -T electrumfair-win ./build.sh
      docker-compose exec -T electrumfair-win rsync -arv --include=ElectrumFair*.exe --exclude=* /opt/wine64/drive_c/electrumfair/contrib/build-wine/dist/ /home/faircoin/bin
    ;;
  "start")
      docker-compose stop php-server
      docker-compose up -d --remove-orphans php-server
      cp ./www/index.php ~/bin/index.php
    ;;
  "stop")
      docker-compose stop php-server
      rm ~/bin/index.php
    ;;
  "remove")
      docker-compose stop
      docker-compose down
      rm ~/bin/index.php
    ;;
esac
