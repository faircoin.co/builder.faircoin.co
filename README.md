# builder

App builder ( electrumfair, faircoin client )

The service is made for/controlled by gitlab runner.

## Usage

Go Gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**

Enter variable name **CMD**

#### CMD - commands

~~~
build-electrumfair-tgz        # build electrumfair source packages .zip and .tar.gz
build-electrumfair-appimage   # build electrumfair .AppImage
build-electrumfair-apk        # build electrumfair apk debug version
build-electrumfair-apk-release # build electrumfair apk release version
build-electrumfair-win        # build electrumfair win version
start                         # start container ( changes of webpage )
stop                          # stop container
remove                        # remove all binaries
~~~


#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
KEYSTORE_PASSWORD           # add this variable manual when build-electrumfair-apk-release
~~~


## Development <small>( manual usage )</small>

If you want create an instance manual then follow the  instructions.

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env
1. run services by ./control.sh

~~~
chmod +x ./control.sh
./control.sh build-electrumfair-tgz
./control.sh build-electrumfair-appimage
./control.sh build-electrumfair-apk
./control.sh build-electrumfair-apk-release
./control.sh build-electrumfair-win
./control.sh start
./control.sh stop
./control.sh remove
~~~
